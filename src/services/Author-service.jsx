import axios from "axios";

export const API = axios.create({baseURL: "http://110.74.194.124:3034/api"});

// get all author
export const getAllAuthor = async ()=>{
    try{
        const result = await API.get("/author");
        console.log("Result :" , result.data.data);
        return result.data.data;
    }catch (err){
        console.log(err);
    }
}

// get author by id 
export const getAuthorById = async (id)=>{
    try{
    const result =  await API.get(`/author/${id}`)
    return result
    }catch(err){
        console.log(err);
    }
}


// delete author by id
export const deleteAuthor = async (id)=>{
    try{
        const result  = await API.delete(`/author/${id}`)
        return result.data.message;
    }catch(err){
        console.log(err);
    }
}
//add author 
export const postAuthor = async (author)=>{
    try{
        const result = await API.post("author", author)
        console.log("FetchAuthor: ", result.data.message);
        return result.data.message
    }catch(err){
        console.log(err);
    }
}

//update article by ID
export const updateAuthorById = async(id, author)=>{
    try {
  
      
      const result = await API.patch(`/author/${id}`, author)
      console.log("upadate:", result.data.data);
      return result.data.data
    } catch (error) {
      console.log("update Error:", error);
    }
  }


  //upload image to api
export const uploadImage = async(image) => {
    try {
     const fd = new FormData();
     fd.append("image", image)
     const result = await API.post("/images", fd, {
         onUploadProgress: progress=>{
             console.log("Uploaded:", Math.floor(progress.loaded / progress.total * 100));
         }
     })
     console.log("uploadImage:", result.data.url);
     return result.data.url
    } catch (error) {
        console.log("uploadImage Error:", error);
    }
 }