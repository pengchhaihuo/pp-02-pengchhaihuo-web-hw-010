import React, { Component } from 'react'
import Author from './components/Author'
import Menu from './components/Menu'

export default class App extends Component {
  render() {
    return (
      <>
        <Menu/>
        <Author/>
      </>
    )
  }
}

