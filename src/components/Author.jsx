import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row ,Col, Form, Button} from 'react-bootstrap'
import Table from './TableData';
import { useEffect, useState } from 'react'
import { getAuthorById, uploadImage,postAuthor,updateAuthorById } from '../services/Author-service';

export default function Author() {

    const [MyImage, setMyImage] = useState(null)
    const [browsedImage, setbrowsedImage] = useState("")
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [isUpdate, setisUpdate] = useState(false)
     

   

    useEffect(async () => {
        if (isUpdate == false) {
            setname("")
            setemail("")
            setbrowsedImage("")
        } else {
            const result = await getAuthorById(1)
            setname(result.data.data.name)
            setemail(result.data.data.email)
            setbrowsedImage(result.image)
        }
    }, [isUpdate])

    // Browse images from and add to image tag
    function onBrowsedImage(e) {
        setMyImage(e.target.files[0]);
        setbrowsedImage(URL.createObjectURL(e.target.files[0]))
    }

    // Add or Update data to api 

    async function onAddAuthor() {
        if (isUpdate == false) {
            const url = MyImage && await uploadImage(MyImage)
            const author = {
                name,
                email,
                image: url ? url : browsedImage
            };
            const result = await postAuthor(author);
            onClearForm()

        } else {
            const url = MyImage && await uploadImage(MyImage)

            const author = {
                name,
                email,
                image: url ? url : browsedImage
            };
            const result = await updateAuthorById(1, author)
        }
    }

    // clear function
    const onClearForm = () => {
        setname("")
        setemail("")
        setbrowsedImage("")
    }


    return (

        <Container>
            <h2>Author</h2>
            <Row>
                <Col md={8}>
                    <Form>
                        <Form.Group >
                            <Form.Label>Author Name</Form.Label>
                            <Form.Control
                                type="text"
                                value={name}
                                onChange={(e) => setname(e.target.value)}
                                placeholder="Author Name" />
                        </Form.Group>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                value={email}
                                onChange={(e) => setemail(e.target.value)}
                                type="email"
                                placeholder="Email" />
                        </Form.Group>
                        <Button
                            onClick={onAddAuthor}
                            variant="primary" > {isUpdate ? "Update" : "Add"}</Button>
                    </Form>
                </Col>
                <Col>

                    <div style={{ width: "250px" }}>
                        <label htmlFor="myfile">
                            <img width="100%" src={browsedImage ? browsedImage : "/images/author-icon.jpg"} />
                        </label>
                    </div>
                    <input
                        onChange={onBrowsedImage}
                        id="myfile"
                        type="file"
                        style={{ display: "none" }}
                    />
                </Col>
            </Row>
            <Row className="my-2">
                <Table onAddAuthor={ onAddAuthor} />
            </Row>
        </Container>
    )
}
