import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button } from 'react-bootstrap'
import { useEffect, useState } from 'react'
import React from 'react'
import { getAllAuthor, deleteAuthor } from '../services/Author-service';



export default function TableData(props) {

    const [author, setAuthor] = useState([])

    useEffect(async () => {
        const result = await getAllAuthor()
        console.log("RESULT", result);
        setAuthor(result)
    }, [])


    const deleteAuthorByID = (id) => {
        const result = deleteAuthor(id)
        let temp = author.filter(item => {
            return item._id !== id
        })
        setAuthor(temp)
    }

 
    return (

        <>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        author.map(
                            (item, index) => (
                                <tr key={index}>
                                    <td>{index}</td>
                                    <td>{item.name}</td>
                                    <td>{item.email}</td>
                                    <td><img src={item.image} width="250px" /></td>
                                    <td>
                                        <Button size="sm"  variant="warning">Edit</Button>{' '}
                                        <Button size="sm" onClick={() => deleteAuthorByID(item._id)} variant="danger">Delete</Button>{' '}
                                    </td>
                                </tr>
                            )
                        )
                    }

                </tbody>
            </Table>
        </>

    )
}

