import 'bootstrap/dist/css/bootstrap.min.css';
import {Navbar,Nav,Button,Form,FormControl} from 'react-bootstrap'
import React from 'react'

export default function Menu() {
    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand href="#home">AMS Management</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="#home">Home</Nav.Link>
                    <Nav.Link href="#link">Post</Nav.Link>
                    <Nav.Link href="#link">User</Nav.Link>
                    <Nav.Link href="#link">Category</Nav.Link>
                    <Nav.Link href="#link">Author</Nav.Link>
                </Nav>
                <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-success">Search</Button>
                </Form>
            </Navbar.Collapse>
        </Navbar>
    )
}
